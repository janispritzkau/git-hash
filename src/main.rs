use std::process::Command;

fn main() {
    let start = std::env::args().skip(1).next().unwrap();

    println!("Searching hashes starting with: {}\n", start);

    let out = String::from_utf8(Command::new("git").args(&["cat-file", "-p", "HEAD"]).output().unwrap().stdout).unwrap();

    let get_user = |line: &str| {
        let start = line.chars().position(|c| c == '<').unwrap();
        let end = line.chars().position(|c| c == '>').unwrap();
        (
            line[line.chars().position(|c| c == ' ').unwrap() + 1..start - 1].to_owned(),
            line[start + 1..end].to_owned(),
            line[end + 2..].to_owned()
        )
    };

    let mut begin = String::new();
    let mut author: Option<(String, String, String)> = None;
    let mut committer: Option<(String, String, String)> = None;

    let mut msg: Option<String> = None;

    for line in out.lines() {
        if let Some(msg) = &mut msg {
            *msg += line;
            *msg += "\n";
        } else if line.starts_with("author") {
            author = Some(get_user(line));
        } else if line.starts_with("committer") {
            committer = Some(get_user(line))
        } else if line.is_empty() {
            msg = Some(String::new())
        } else {
            begin += line;
            begin += "\n";
        }
    }

    let author = author.unwrap();
    let committer = committer.unwrap();
    let mut msg = msg.unwrap();
    msg.pop();

    begin += &format!("author {} <{}> {}\n", author.0, author.1, author.2);

    let done = std::sync::Arc::new(std::sync::Mutex::new(false));

    for j in 0..num_cpus::get() as u64 {
        let begin = begin.clone();
        let msg = msg.clone();
        let author = author.clone();
        let committer = committer.clone();
        let done = done.clone();
        let start = start.clone();

        let rand = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_secs() % 1_000_000;

        std::thread::spawn(move || {
            let mut i: u64 = rand * 2548062 + j * 85743089234;
            loop {
                let mut text = begin.clone();
                text += &format!("committer {} {i:x} <{}> {}\n\n{}\n", committer.0, committer.1, committer.2, &msg, i=i);

                let hash = sha1::Sha1::from(format!("commit {}\0{}", text.len(), text)).hexdigest();
                if hash.starts_with(&start) {
                    println!("Hash: {}", hash);
                    println!(r#"GIT_COMMITTER_NAME="{} {i:x}" GIT_COMMITTER_EMAIL="{}" GIT_COMMITTER_DATE="{}" git commit --author "{} <{}>" --date="{}" -m "{}""#, committer.0, committer.1, committer.2, author.0, author.1, author.2, msg, i=i);
                    let mut done = done.lock().unwrap();
                    *done = true;
                    break
                }

                i += 1;
            }
        });
    }
    loop {
        if *done.lock().unwrap() { break }
        std::thread::sleep(std::time::Duration::from_millis(50))
    }
}
