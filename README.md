# Git Hash

Finds a git hash with your desired beginning.

```sh
# execute in a git repo folder
git-hash 123456
# undo last commit
# output of program will give you a command that will redo the commit
```
